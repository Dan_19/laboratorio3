def winner(deck_steve, deck_josh):
    
    josh_score = 0
    steve_score = 0
    
    deck = { 
    '2': 0,
    '3': 1,
    '4':2,
    '5':3,
    '6':4,
    '7':5,
    '8':6,
    '9':7,
    'T':8,
    'J':9,
    'Q':10,
    'K':11,
    'A':12
    }
    
    for i in range(len(deck_steve)):
        steve_card = deck_steve[i]
        josh_card = deck_josh[i]
        
        if deck[steve_card] > deck[josh_card]:
            steve_score+=1
        elif deck[steve_card] < deck[josh_card]:
            josh_score+=1
            