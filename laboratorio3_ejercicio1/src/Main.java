import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        String word = "Hola";

        System.out.println( solution(word));

    }

    public static String solution(String str) {

        String wordBackguards = "";

        for(int i = str.length() - 1; i >= 0 ; i--){

            wordBackguards += str.charAt(i);

        }

        return wordBackguards;
    }


}